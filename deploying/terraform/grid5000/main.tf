terraform {
  required_version = ">= 0.13.0"
  required_providers {
    grid5000 = {
      source  = "pmorillon/grid5000"
      version = "~> 0.0.8"
    }
  }
}

provider "grid5000" {
  # API credentials for external usages
  username = var.g5k_username
  password = var.g5k_password
}


resource "grid5000_job" "my_g5k_job" {
  name      = "terraform_demo"
  site      = "grenoble"
  command   = "sleep 2h"
  resources = "{cluster='yeti'}/nodes=1"
  types     = ["exotic"]
}

output "my_g5k_machines" {
  value = {
    assigned_nodes = grid5000_job.my_g5k_job.assigned_nodes,
    g5k_site       = grid5000_job.my_g5k_job.site,
    g5k_job_id     = grid5000_job.my_g5k_job.id,
  }
}
