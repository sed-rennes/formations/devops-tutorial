# DevOps tutorial by SED-RBA

This repository contains slides and example code for the DevOps tutorial hosted by SED-RBA.

More information on the training:

- https://sed-rennes.gitlabpages.inria.fr/formations/web/agenda/
- https://intranet.inria.fr/Actualite/Formation-SED-Introduction-au-DevOps-pour-la-recherche-experimentale-13-septembre-2023

## Description

Experimental research in computer science requires more and more complex software, and this software needs to run in complex environments. To keep experiments manageable, three properties are desirable: automation, tracability and reproducibility.

In the industry, DevOps has become an established set of methodologies to deploy and manage complex software systems in production. Realizing that DevOps shares some of the goals of experimental research, the goal of this training day is to present existing DevOps tools and methodologies and explain how they can apply to experimental research.

During the day, several engineers or PhD students will share tools, use-cases and practical feedback on the following subjects:

- introduction to DevOps and how it can apply to experimental research

- building and packaging software reproducibly (Docker, Packer, Guix, Nix, Gitlab-CI...)

- deploying experimental code on research platforms (Terraform, Ansible, Enoslib)

- monitoring an experiment (Prometheus, Enoslib, Grid'5000)

If you are thinking "I've applied good practices to develop my research code, now I want to deploy it and experiment with it", then you are the right target for this training day!

## Detail of the sessions

- 9h30-9h35 (Eric Poiseau) Foreword

- 9h35-9h50 (Baptiste Jonglez) [Introduction to DevOps and program for the day](slides/1-devops-intro.pdf). Associated demo: [Docker examples](building/docker/)

- 9h50-10h45 (Baptiste Jonglez) [Packaging software and reproducibility](slides/2-packaging.pdf)

- 10h45-11h (Matthieu Simonin) [Packer demo](building/packer/)

- break

- 11h20-11h50 (Pierre Neyron, remotely) [Kameleon: an OS appliance builder](slides/kameleon-SED-Rennes.pdf)

- 11h50-12h30 (Volodia Parol-Guarino) [Nix: Make reproducible, declarative and reliable systems](slides/NixSED.pdf). Associated demo: [Nix demo](building/nix/)

- lunch break

- 14h-14h30 (Matthieu Simonin) [GUIX demo](building/guix/)

- 14h30-14h45 (Baptiste Jonglez) [Introduction to deployment](slides/3-deploy.pdf)

- 14h45-16h (Matthieu Simonin and Guillermo Andrade Barroso) [Real-world deployment of Overleaf with Ansible and Docker-compose](slides/ansible.md)

- break

- 16h15-17h (Baptiste Jonglez) [Terraform demo](deploying/terraform/)

- skipped: [Enoslib demo](deploying/enoslib/)
