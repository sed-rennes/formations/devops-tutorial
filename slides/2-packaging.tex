\documentclass[14pt,xcolor=dvipsnames,presentation,aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}

% Font
%\usepackage[default]{lato}
%\usepackage{helvet}
%\usepackage[default]{cantarell}
%\usepackage[sfdefault]{biolinum}
%\usepackage{cmbright}
\usepackage[sfdefault,lf]{carlito}
\renewcommand*\oldstylenums[1]{\carlitoOsF #1}


\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage[export]{adjustbox}[2011/08/13]
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usedescriptionitemofwidthas{bl}
\usepackage{ifthen,figlatex,amsmath,amstext,xspace}
\usepackage{boxedminipage,xspace,multicol}
\usepackage{subfigure}
\usepackage{algorithm2e}
\usepackage{hhline}
\usepackage{multirow}
\usepackage{booktabs}

%\usetheme{Madrid}
%\usecolortheme[named=BrickRed]{structure}

\useoutertheme{default}
\useinnertheme{default}

\setbeamercolor*{frametitle}{fg=orange,bg=white}
\setbeamercolor*{lower separation line head}{bg=DeepSkyBlue4} 
\setbeamercolor*{normal text}{fg=black,bg=white} 
\setbeamercolor*{alerted text}{fg=red} 
\setbeamercolor*{example text}{fg=black} 
%\setbeamercolor*{structure}{fg=black} 
% 
\setbeamercolor*{palette tertiary}{fg=black,bg=black!10} 
\setbeamercolor*{palette quaternary}{fg=black,bg=black!10} 
%
\definecolor{my-light-blue}{HTML}{D3DCEC}
\definecolor{my-dark-blue}{HTML}{093A7A}
\definecolor{my-dark-blue2}{HTML}{00688B}
\definecolor{my-light-red}{HTML}{FFBABA}
\definecolor{my-dark-red}{HTML}{D51111}
%\definecolor{my-light-orange}{HTML}{F2B76E}
\definecolor{my-light-orange}{HTML}{FFD8B2}
%\definecolor{my-dark-orange}{HTML}{FFA500}
%\definecolor{my-dark-orange}{HTML}{CF7C00}
\definecolor{my-dark-orange}{HTML}{FE8D00}
\definecolor{my-light-green}{HTML}{BFFFBF}
\definecolor{my-dark-green}{HTML}{00B000}
\definecolor{my-light-grey}{HTML}{E6E6E6}

\setbeamertemplate{blocks}[rounded]
\setbeamercolor{block body}{bg=my-light-orange}
\setbeamercolor{block title}{bg=my-dark-orange,fg=white}
\setbeamercolor{block body alerted}{bg=my-light-red}
\setbeamercolor{block title alerted}{bg=my-dark-red,fg=white}
\setbeamercolor{block body example}{bg=my-light-green}
\setbeamercolor{block title example}{bg=my-dark-green,fg=white}

\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}

% \usepackage[colorlinks=true,citecolor=pdfcitecolor,urlcolor=pdfurlcolor,linkcolor=pdflinkcolor,pdfborder={0 0 0}]{hyperref}

\usepackage[round-precision=3,round-mode=figures,scientific-notation=true]{siunitx}
\usetheme{default}
\date{13 September 2023}
\title{Introduction to packaging}
\hypersetup{
 pdfauthor={Baptiste Jonglez},
 pdftitle={Introduction to packaging},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={},
 pdflang={English}}


\begin{document}

\urlstyle{sf}
\let\alert=\structure
\let\epsilon=\varepsilon
\let\leq=\leqslant
\let\geq=\geqslant


\author{Baptiste Jonglez, Inria}

\maketitle

\begin{frame}{Packaging}

{\Large Goal: package \textbf{your} software (or fork of existing software) to perform experiments.}

\vspace{1cm}

Several complementary levels:

\begin{itemize}
\item language-specific packaging and dependency tracking (Python setuptools, Rust Cargo, Go, CMake, etc)
\item \textbf{OS-level dependencies} (Docker, Nix, Guix)
\item \textbf{low-level dependencies on kernel or hardware} (virtual machine or bare metal image)
\end{itemize}
\end{frame}

\begin{frame}{Dockerfile demo}
\Large See building/garage in the tutorial git repository.
\end{frame}

\begin{frame}{Reproducibility}
\begin{quote}
“A research work is called reproducible if all information relevant to the work, including, but not limited to, text, data and code, is made available, such that an independent researcher can reproduce the results” (Vandewalle, Kovacevic, and Vetterli 2009)
\end{quote}

\vspace{1cm}

Reproducibility crisis in science: see Arnaud Legrand's work such as \url{https://rr-france.github.io/bookrr/A-introduction.html}
\end{frame}

\begin{frame}{Reproducibility: artifacts}
\textbf{Software artifacts:} source code, built image

\vspace{1cm}

Research repositories: Software Heritage (source), Zenodo (binary
artifacts).  Both allow easy citation for your papers.
\end{frame}

\begin{frame}{Reproducibility: rebuilding artifacts}
You could create your artifacts manually.

\vspace{1cm}

Why rebuilding artifacts is important:

\begin{itemize}
\item build artifacts can disappear (e.g. on Docker Hub)
\item extending scientific work (even your own work!) requires making
  changes and rebuilding
\end{itemize}
\end{frame}

\begin{frame}{Reproducibility: limits of Docker (1)}
\Large Exercice: find reproducibility issues in building/matrix-synapse/Dockerfile
\end{frame}

\begin{frame}{Reproducibility: limits of Docker (2)}
\begin{itemize}
\item base image (debian:buster) is not fixed $\rightarrow$ \textit{can put a hash}
\item Debian full-upgrade $\rightarrow$ \textit{avoid}
\item installing latest version of Debian packages $\rightarrow$ \textit{snapshot.debian.org}
\item Debian package repository will stop working at some point $\rightarrow$ \textit{idem}
\item no Python dependency pinning $\rightarrow$ \textit{lockfile / pip freeze}
\item download external software $\rightarrow$ \textit{Software Heritage}
\item copy local files into images $\rightarrow$ \textit{build in Gitlab CI}
\end{itemize}
\end{frame}

\begin{frame}{Reproducibility and DevOps}
\textbf{DevOps tools are usually not targeting reproducibility}

\vspace{1cm}

Hypothesis: they focus on rapid iteration cycles $\rightarrow$ old
versions are obsoleted very quickly
\end{frame}

\begin{frame}{Reproducibility: references}
Some references here: \url{https://rr-france.github.io/bookrr/C-code-env.html}
\end{frame}


\begin{frame}{Packaging (repeated)}

{\Large Goal: package \textbf{your} software (or fork of existing software) to perform experiments.}

\vspace{1cm}

Several complementary levels:

\begin{itemize}
\item language-specific packaging and dependency tracking (Python setuptools, Rust Cargo, Go, CMake, etc)
\item \textbf{OS-level dependencies} (Docker, Nix, Guix)
\item \textbf{low-level dependencies on kernel or hardware} (virtual machine or bare metal image)
\end{itemize}
\end{frame}

\begin{frame}{Next steps}
\begin{itemize}
\item full-image generation: Packer, Kameleon
\item reproducible environment creation: Nix, Guix
\end{itemize}
\end{frame}

\end{document}
