# About

This deploys virtual machines on an OpenStack instance using Terraform.

# Getting started

## First time install

- Install Terraform
- Run `terraform init`
- Change login and project in `main.tf`

## Creating / recreating VMs

- `export OS_PASSWORD="yourpassword"`
- Run `terraform plan`, `terraform apply`
- The floating IP of your VMs should be printed on stdout
- You can SSH with `ssh ubuntu@<floating_ip>` with your regular RSA SSH key
- Run `terraform destroy` to release VMs
