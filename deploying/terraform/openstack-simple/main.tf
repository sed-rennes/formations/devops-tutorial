# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# Configure the OpenStack Provider
# Note: you need to define OS_PASSWORD before calling terraform
# TODO: read password from a file that is ignored by git
provider "openstack" {
  user_name   = "your_login"
  tenant_name = "your_password"
  auth_url    = "https://url-of-your-openstack-instance"
}

# Internal network & security group
resource "openstack_networking_network_v2" "internal" {
  name           = "internal"
  admin_state_up = "true"
}
resource "openstack_networking_subnet_v2" "subnet" {
  name            = "subnet"
  network_id      = openstack_networking_network_v2.internal.id
  cidr            = "192.168.42.0/24"
  ip_version      = 4
  dns_nameservers = ["192.44.75.10", "192.108.115.2"]
}
resource "openstack_networking_secgroup_v2" "sg-open" {
  name        = "sg-open"
  description = "Open security group, allows everything"
}
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_all" {
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.sg-open.id
}

# External network/router
resource "openstack_networking_router_v2" "router" {
  name                = "router"
  admin_state_up      = true
  external_network_id = "7af4393b-6556-4d29-b68e-107d02709b96" # external
}
resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}

# SSH key pair
resource "openstack_compute_keypair_v2" "sshkey" {
  name       = "sshkey"
  public_key = file("~/.ssh/id_rsa.pub")
}

# Create a test VM
resource "openstack_compute_instance_v2" "test-server" {
  name = "test"
  #image_name      = "ubuntu-22.04"
  image_name      = "imta-ubuntu22"
  flavor_name     = "s10.large"
  key_pair        = "sshkey"
  security_groups = ["sg-open"]
  network {
    uuid = openstack_networking_network_v2.internal.id
  }
}

# Floating IP to access the VM through SSH
resource "openstack_networking_floatingip_v2" "test-server-ip" {
  pool = "external"
}
resource "openstack_compute_floatingip_associate_v2" "test-server-ip" {
  floating_ip = openstack_networking_floatingip_v2.test-server-ip.address
  instance_id = openstack_compute_instance_v2.test-server.id
}

output "instance_ip_addresses" {
  value = {
    for ip in [openstack_compute_floatingip_associate_v2.test-server-ip] :
    ip.instance_id => ip.floating_ip
  }
}

