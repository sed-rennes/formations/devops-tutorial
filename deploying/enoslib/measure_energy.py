import getpass
import logging
from pathlib import Path
import time
import sys

import requests
import requests.auth

import enoslib as en


CLUSTER = "nova"


api_username = None
api_password = None

def g5k_api_auth():
    global api_username
    global api_password
    if api_username is None:
        api_username = getpass.getpass("G5K username: ")
    if api_password is None:
        api_password = getpass.getpass("G5K password: ")
    return (api_username, api_password)

# Modified from https://www.grid5000.fr/w/Energy_consumption_monitoring_tutorial
def get_power_values(node, site, start, stop, metric="wattmetre_power_watt"):
    username, password = g5k_api_auth()
    basic_auth = requests.auth.HTTPBasicAuth(username, password)
    url = "https://api.grid5000.fr/stable/sites/%s/metrics?metrics=%s&nodes=%s&start_time=%s&end_time=%s" \
            % (site, metric, node, int(start), int(stop))
    req = requests.get(url, auth=basic_auth)
    if not req.ok:
        print(req.text, file=sys.stderr)
        return []
    data = req.json()
    return data

def run_command_monitored(command, host, **kwargs):
    """Run the specified command on the specified host, while taking power
    measurements.  Return the average power consumption.  kwargs are
    passed to run_command.
    """
    start = time.time()
    en.run_command(command, roles=[host], **kwargs)
    stop = time.time()
    node, site = host.address.split(".")[:2]
    power_values = get_power_values(node, site, start, stop)
    # Remove first value, because it may have been measured before starting the command
    power_values = power_values[1:]
    if len(power_values) == 0:
        return 0
    return sum(item["value"] for item in power_values) / len(power_values)


# Initialize Enoslib
en.init_logging(level=logging.INFO)
en.check()

# Ask for auth credentials for G5K API (to get power consumption metrics)
g5k_api_auth()

# Define job
job_name = Path(__file__).name

conf = (
    en.G5kConf.from_settings(job_name=job_name, walltime="0:45:00")
    .add_machine(roles=["idle"], cluster=CLUSTER, nodes=1)
    .add_machine(roles=["monothread"], cluster=CLUSTER, nodes=1)
    .add_machine(roles=["multithread"], cluster=CLUSTER, nodes=1)
)

# This will validate the configuration, but not reserve resources yet
provider = en.G5k(conf)

# Get actual resources
roles, networks = provider.init()

# Make sure node-exporter is disabled on all machines, because it generate
# power consumption spikes due to high CPU usage.
en.run_command("systemctl stop prometheus-node-exporter.service", roles=roles)

# Measure power consumption while running commands
idle_power = run_command_monitored("sleep 30", roles["idle"][0])
print("Idle ({}): {} W".format(CLUSTER, int(idle_power)))

monothread_power = run_command_monitored("stress -c 1 -t 30", roles["monothread"][0])
print("1 CPU core ({}): {} W".format(CLUSTER, int(monothread_power)))

# gather_facts is needed to have access to fact variables such as the number of threads
multithread_power = run_command_monitored("stress -c {{ansible_processor_vcpus}} -t 30",
                                          roles["multithread"][0],
                                          gather_facts=True)
print("All CPU cores ({}): {} W".format(CLUSTER, int(multithread_power)))

# Release all Grid'5000 resources
#provider.destroy()
