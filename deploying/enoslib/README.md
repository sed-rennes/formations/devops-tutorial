# Enoslib examples

Documentation: https://discovery.gitlabpages.inria.fr/enoslib/index.html

Getting started: https://discovery.gitlabpages.inria.fr/enoslib/tutorials/grid5000.html#installation

Then, from your virtualenv, you should be able to run the scripts:

    python3 measure_energy.py

It should give you the actual measured power consumption of [nova](https://www.grid5000.fr/w/Lyon:Hardware#nova)
nodes in Lyon while idle, with a single CPU core working at 100%, and with all CPU cores working at 100%.
