

# Overleaf deployment using Ansible + Docker

Matthieu Simonin, Guillermo Andrade
September 13th, 2023

---

## Architecture overview (IRISA):

 ```mermaid
graph LR


subgraph DMZ[DMZ]
    qlf-overleaf.irisa.fr -- alias DNS--> proxy1
    overleaf.irisa.fr -- alias DNS--> proxy2

    test[test-overleaf.irisa.fr]
    subgraph RP[Reverse proxy]
        proxy1[proxy-overleaf.irisa.fr]
        proxy2[proxy-overleaf.irisa.fr]
    end
    style RP color:#f66
end

subgraph internet
    client[python-sharelatex]
    user[user browser]
end


subgraph intranet
    prod[prod-overleaf.irisa.fr]
end

user-- https:443 --> overleaf.irisa.fr
client-- https:443 --> overleaf.irisa.fr
proxy2-- reverse proxy : 443-->prod
proxy1-- reverse proxy : 443-->test
```

---

## Builds



- [overleaf docker](https://gitlab.inria.fr/sed-rennes/overleaf/overleaf-monorepo/-/blob/dev/inria-3.x/.gitlab-ci.yml): contains all the overleaf services(~10 services) in a single docker
    - rebuild automatically when patching (we have local patches) of overleaf code.
- [the (badly reproducible)compilation docker](https://gitlab.inria.fr/sed-rennes/overleaf/compile_worker/-/blob/master/Dockerfile): texlive distributions (one per year). 

---

## Docker-compose

Local deployment consists in several dockers:
- overleaf
- mongo
- redis
- prometheus/grafana


```bash
ENV=test-overleaf.irisa.fr docker compose  -f docker-compose.yml -f envs/${ENV}/docker-compose.yml up -d
# ENV=test-overleaf.irisa.fr make upd
```

```yaml
version: '2.2'
services:
  sharelatex:
    env_file:
     - envs/${ENV}/overleaf.env
     # The following defines some shared secret between overleaf and mongo
     # SHARELATEX_MONGO_URL
     # MONGO_SHARELATEX_USER
     # MONGO_SHARELATEX_PASSWORD
     - envs/${ENV}/mongo_overleaf.env
    restart: always
    # Default image to use (overrides in envs)
    image: sharelatex/sharelatex:3.0.1
    container_name: sharelatex
    depends_on:
      mongo:
        condition: service_healthy
      redis:
        condition: service_started
    links:
    - mongo
    - redis
    volumes:
    - /var/lib/overleaf:/var/lib/overleaf
    - /var/lib/overleaf-scratch:/var/lib/overleaf-scratch
    - ./settings.js:/etc/sharelatex/settings.js
    - /var/run/docker.sock:/var/run/docker.sock

  mongo:
    env_file:
     - envs/${ENV}/mongo.env
     - envs/${ENV}/mongo_overleaf.env
    restart: always
    # For sharelatex 2.5.x, mongodb 4 is recommended
    # fixing to something reasonnable (4.4 is taken as an examples in the doc)
    image: mongo:4.4
    container_name: mongo
    ports:
    - 27017:27017
    volumes:
    - /var/lib/mongo:/data/db
    # NOTE(msimonin)
    # Mounting this as this will trigger the execution of all the script inside the directory
    # This is a good place to put some initial user creation...
    - ./library/mongo/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
    healthcheck:
        test: echo 'db.stats().ok' | mongo localhost:27017/test --quiet
        interval: 10s
        timeout: 10s
        retries: 5

  redis:
    restart: always
    image: redis:5
    container_name: redis
    expose:
    - 6379
    volumes:
    - /var/lib/redis:/data/db

  overleaf_ops:
    restart: always
    build:
      context: docker
      dockerfile: Dockerfile.cron
    container_name: overleaf_ops
    volumes:
    - /var/lib/overleaf:/var/lib/overleaf:ro
    - /var/lib/overleaf_backup:/backup
    - /var/run/docker.sock:/var/run/docker.sock
    env_file:
     - envs/${ENV}/mongo_overleaf.env

  prometheus:
    restart: always
    image: prom/prometheus
    container_name: prometheus
    links:
    - sharelatex
    - node_exporter
    ports:
    - 127.0.0.1:9090:9090
    expose:
    - 9090
    volumes:
    - ./prometheus.yml:/etc/prometheus/prometheus.yml

  node_exporter:
    restart: always
    image: quay.io/prometheus/node-exporter:latest
    container_name: node_exporter
    command:
      - '--path.rootfs=/host'
    pid: host
    restart: unless-stopped
    volumes:
      - '/:/host:ro,rslave'

  grafana:
    restart: always
    image:  grafana/grafana-oss
    container_name: graphana
    ports:
    - 127.0.0.1:3000:3000
    links
    - prometheus
```

:warning: some configurations are required outside the `docker-compose` file:
    - Network mounts for the data dirs
    - Managements of the host machine (security updates, mail server, certificates)
    - Pre pull of compilation images


---

## From single-machine deployment to multi environment deployement.

We have 3 kinds of environnment:
- `test` docker-compose only
- `prod` + `qualif`: remote machines that require priors configurations
    - => we use Ansible to prepare the machines, generate the right environment files and launch `docker-compose`

---

## Ansible

```bash
ansible-playbook -e @secret.yaml -i test-overleaf.irisa.fr.yml site.yml
```

---

### Overview

```mermaid
%%{init: { 'logLevel': 'debug', 'theme': 'light', securityLevel: 'loose'} }%%

graph LR


subgraph roles[roles]
    overleaf
    proxy
end
subgraph tasks[Tasks groups]
    prepare
    setup
    post-install
    proxy_tasks[main]
end
overleaf-->prepare
overleaf-->setup
overleaf-->post-install

subgraph configuration constants
    gitlab_app_id[Gitlab Secrets]
    sharelatex_bot_password[bot password]
    hosts
end

subgraph Tasks
    apt_install[apt install]
    config_smtp[config mail server]
    install_docker[Install docker]
    create_files_struct[create overleaf data structure]
    docker_up[docker compose up]
    cron_task[program cron task]
    virhost[virtual host configuration]
    
end
overleaf.conf.j2-->virhost
virhost-->virt_host_conf[virtualhost.conf]
hosts-->virt_host_conf
proxy_tasks-->virhost
prepare-->apt_install
prepare-->config_smtp
setup-->create_files_struct
setup-->install_docker
setup-->docker_up
post-install-->cron_task
proxy-->proxy_tasks
secret.yml-->gitlab_app_id
secret.yml-->sharelatex_bot_password
test-overleaf.irisa.fr.yml
prod-overleaf.irisa.fr.yml-->hosts
site.yml
site.yml-->overleaf
site.yml-->proxy
test-overleaf.irisa.fr.yml-->hosts
create_files_struct-->overleaf.env
gitlab_app_id-->overleaf.env

```

---

### Site

```yaml
---
- name: Overleaf deploy
  hosts: overleaf
  roles:
  - role: overleaf
    tags: [overleaf]

- name: Apache proxy
  hosts: proxy
  roles:
  - role: proxy
    tags: [proxy]
```
### Inventory (Prod)

in file `prod-overleaf.irisa.fr.yml`:
```yaml
overleaf:
  hosts:
    prod-overleaf.irisa.fr:
      ssh:
        ssh_args : '-o ForwardAgent=yes -o StrictHostKeyChecking=no'
      ansible_user: root
      sharelatex_app_name: Overleaf Inria Edition
      # The public name accessible from outside
      # This should be overleaf.irisa.fr 
      sharelatex_server_name: "overleaf.irisa.fr" 
      # The internal name (not exposed to the internet)
      # would be prod-overleaf.irisa.fr
      sharelatex_virtual_host: "{{ inventory_hostname }}"
      sharelatex_behind_proxy: true
      sharelatex_secure_cookie: true
      sharelatex_site_url: "https://overleaf.irisa.fr"
      sharelatex_nav_title: Overleaf Inria Edition
      # user for compilation test
      sharelatex_bot_username: support-overleaf@inria.fr
```

### Task examples

#### install packages
```yaml
- name: install postfix
  apt: name=postfix state=present
  
- name: add GPG key
  apt_key:
    url: https://download.docker.com/linux/ubuntu/gpg
    state: present
- name: add docker repository to apt
  apt_repository:
    repo: deb https://download.docker.com/linux/ubuntu bionic stable
    state: present    
- name: install docker
  apt:
    name: "{{item}}"
    state: latest
    update_cache: yes
  loop:
    - docker-ce
    - docker-ce-cli
    - containerd.io
```
#### generate file from template

```yaml
- name: Generate virtual host configuration
  template:
    src: overleaf.conf.j2
    dest: /etc/apache2/sites-available/{{ virtual_host }}.conf
```
##### Template file (Jinja2):
```htmlmixed
<VirtualHost *:80>
  ServerName {{ server_name }}

  redirect permanent / https://{{ server_name }}/
</VirtualHost>

<VirtualHost *:443>
  ServerName {{ server_name }}

  LogLevel warn

  SSLEngine on
  {% if ssl_self_signed is defined -%}
  SSLCertificateFile /etc/ssl/certs/{{ server_name }}.crt.self_signed
  SSLCertificateKeyFile /etc/ssl/private/{{ server_name }}.key.self_signed
  {% else -%}
  SSLCertificateFile /etc/ssl/certs/{{ server_name }}.crt
  SSLCertificateKeyFile /etc/ssl/private/{{ server_name }}.key
  SSLCertificateChainFile /etc/apache2/ssl.crt/cacerts.pem
  {% endif %}
...
```
##### Services
```yaml
- name: be sure that docker is started
  service:
    name: docker
    state: started
    enabled: yes
```

##### Set a line in a file
```yaml
- name : Configure email for unattended updgrades
  lineinfile:
    path: /etc/apt/apt.conf.d/50unattended-upgrades
    regexp: '^Unattended-Upgrade::Mail'
    line: 'Unattended-Upgrade::Mail "support-overleaf@inria.fr";'
```

#### Overleaf config (template)

```bash
...
SHARELATEX_APP_NAME={{ sharelatex_app_name }}
...
SHARELATEX_BEHIND_PROXY={{sharelatex_behind_proxy}}

{% if sharelatex_secure_cookie %}
# settings are only checking if it's set and set it regardless the actual value
SHARELATEX_SECURE_COOKIE=true
{% endif %}
...
SHARELATEX_SITE_URL={{ sharelatex_site_url }}
SHARELATEX_NAV_TITLE={{ sharelatex_nav_title }}
SHARELATEX_ADMIN_EMAIL={{ email_address }}
SHARELATEX_HEADER_EXTRAS='{{ sharelatex_header_extras }}'
...

TEX_LIVE_DOCKER_IMAGE={{ tex_live_docker_image }}
...
ALL_TEX_LIVE_DOCKER_IMAGE_NAMES={{ all_tex_live_docker_image_names }}
# descriptions
ALL_TEX_LIVE_DOCKER_IMAGES={{ all_tex_live_docker_images}}
# Make sure the two above are found
# This still NEEDs ``settings.imageRoot`` in the settings
DOCKER_IMAGE_ROOT=registry.gitlab.inria.fr/sed-rennes/overleaf/compile_worker
...
GITLAB_APP_CALLBACK_URL={{gitlab_app_callback_url}}
GITLAB_APP_BASE_URL={{gitlab_app_base_url}}
GITLAB_APP_ID={{gitlab_app_id}}
GITLAB_APP_SECRET={{gitlab_app_secret}}
```


