# Building a custom version of Garage

The goal here is to build a custom version of [Garage](https://garagehq.deuxfleurs.fr),
a self-hosted distributed object storage software.

As a first step for reproducibility, we build Garage within a Docker container.

## Understanding

Look at the Dockerfile.

We use a multi-stage build to produce a smaller final image that does not contain all the Rust
compiler toolchain.  This is a good practice in the industry, and also useful in research to
speed up the deployment of your experiments.

Note that we clone the git repository of Garage inside the container.  For your own software
(or forks of existing software), you would typically use the source code of the current repository.

## Building

    docker build -t garage .

Building should take a few minutes depending on your Internet connection and CPU speed.

## Test running

    docker run -it garage:latest /garage help

This should output Garage options.
