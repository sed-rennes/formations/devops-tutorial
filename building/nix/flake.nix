{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    gohelloworld.url = "github:volodiapg/gohtmxhelloworld/main";
    gohelloworld.inputs.nixpkgs.follows = "nixpkgs";
    gohelloworld.inputs.flake-utils.follows = "flake-utils";
  };

  outputs = inputs:
    with inputs; let
      inherit (self) outputs;
      flakeDevEnv = flake-utils.lib.eachDefaultSystem (
        system: let
          pkgs = import nixpkgs {
            inherit system;
          };
        in {
          formatter = pkgs.alejandra;
          devShells.default = pkgs.mkShell {
            packages = with pkgs; [
              just
              qemu
              sshpass
            ];
          };
        }
      );
      flakeDocker = flake-utils.lib.eachDefaultSystem (
        system: let
          pkgs = import nixpkgs {
            inherit system;
          };

          dockerImage = pkgs.dockerTools.buildImage {
            name = "tutosed";
            tag = "latest";

            config = {
              Entrypoint = ["${gohelloworld.packages.${system}.default}/bin/counter-basic"];
            };
          };
        in {
          packages.docker = dockerImage;
        }
      );
      flakeVM = flake-utils.lib.eachSystem ["x86_64-linux"] (
        system: let
          pkgs = import nixpkgs {
            inherit system;
          };
          os = nixpkgs.lib.nixosSystem {
            inherit system;
            modules = [
              "${nixpkgs}/nixos/modules/profiles/qemu-guest.nix"
              "${nixpkgs}/nixos/modules/profiles/all-hardware.nix"
              outputs.nixosModules.vmConfig
            ];
          };
          vm = import "${nixpkgs}/nixos/lib/make-disk-image.nix" {
            inherit pkgs;
            inherit (pkgs) lib;
            inherit (os) config;
            memSize = 4096; # During build-phase, here, locally
            additionalSpace = "2G"; # Space added after all the necessary
            format = "qcow2-compressed";
          };
        in {
          packages.vm = vm;
        }
      );
      flakeModules = {
        nixosModules.vmConfig = {
          pkgs,
          lib,
          ...
        }: {
          fileSystems."/" = {
            device = "/dev/disk/by-label/nixos";
            fsType = "ext4";
          };

          boot = {
            growPartition = true;
            kernelParams = ["console=ttyS0"]; # "preempt=none"];
            loader.grub = {
              device = "/dev/vda";
            };
            loader.timeout = 0;
          };

          users.mutableUsers = false;
          users.users.root = {
            isSystemUser = true;
            password = "root";
          };

          services.openssh = {
            enable = true;
            settings.PermitRootLogin = "yes";
          };

          environment.systemPackages = [
            pkgs.firefox
            pkgs.browsh
          ];

          systemd.services.helloworld = {
            description = "Launch our hello world";
            after = ["networking.target"];
            wantedBy = ["multi-user.target"];

            serviceConfig = {
              Type = "oneshot";
              RemainAfterExit = "yes";
              ExecStart = "${gohelloworld.packages.${pkgs.system}.default}/bin/counter-basic";
            };
          };

          system.stateVersion = "22.05"; # Do not change
        };
      };
    in
      nixpkgs.lib.foldl nixpkgs.lib.recursiveUpdate {}
      [
        flakeDevEnv
        flakeDocker
        flakeModules
        flakeVM
      ];
}
