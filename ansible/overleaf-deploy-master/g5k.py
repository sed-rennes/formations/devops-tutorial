"""
Deploy all the thing on g5k:
you'll need to install enoslib

.. code-block:: bash

    pip install -U enoslib

Using IPv6 you can connect from your local machine to the service running on a
g5k node directly (no SSH tunnel required).
This IPv6 is displayed at the end of the deployment.

Manual step:
    add an entry to your /etc/hosts file to access the service
    (otherwise gitlab oauth redirect won't work)

.. code-block:: bash

    <ipv6>      sharelatex.local

"""
import logging
from pathlib import Path
import time

import enoslib as en


en.init_logging()

job_name = Path(__file__).name


# some texlive image to compile the projects
# COMPILE_IMAGES = ["registry.gitlab.inria.fr/sed-rennes/overleaf/compile_worker/compile_worker:2020.1001",
#                  "registry.gitlab.inria.fr/sed-rennes/overleaf/compile_worker/compile_worker:2021.1001"]
COMPILE_IMAGES = []

def deploy(roles, networks, registry_opts):
    docker = en.Docker(agent=roles["overleaf"], bind_var_docker="/tmp/docker", registry_opts=registry_opts)
    docker.deploy()

    with en.actions(roles=roles["overleaf"]) as a:
        # synchronize the current directory
        a.synchronize(
            src=str(Path.cwd()),
            dest="/tmp/overleaf",
            task_name="Copying local files to remote machine"
        )
        # prepare drectory structure for overleaf data
        for p in ["data/user_files", "data/compiles", "data/cache", "data/template_files", "tmp/dumpFolder", "uploads"]:
            a.file(path=f"/var/lib/overleaf/{p}", owner="33", group="33", state="directory", recurse="true", task_name="Preparing directories")

        for image in COMPILE_IMAGES:
            a.docker_image(name=image, source="pull", task_name="Downloading compile image")

        a.shell("which docker-compose || (curl -L 'https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64' -o /bin/docker-compose && chmod a+x /bin/docker-compose)",
        task_name="Installing docker_compose")
        # Create remote directory structure

        a.shell("ENV=g5k make upd", chdir="/tmp/overleaf/overleaf-deploy", task_name="Starting up !")
        a.wait_for(port=443, state="present")



def g5k():
    conf = (
        en.G5kConf.from_settings(job_name=job_name, project="myriads", walltime="04:00:00")
        .add_machine(
            roles=["overleaf"], cluster="paravance", nodes=1
        )
        .add_machine(
            roles=["apache"], cluster="paravance", nodes=1
        )
    )

    provider = en.G5k(conf)
    roles, networks = provider.init()

    registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)

    deploy(roles, networks, registry_opts)
    apache(roles, networks)

    with provider.firewall(hosts=roles["overleaf"], port=443):
        en.run("dhclient -6 br0", roles=roles["overleaf"])
        result = en.run("ip -6 addr show dev br0", roles=roles["overleaf"])
        print(f"Overleaf is available on IPV6: {result[0].stdout}")
        print(f"You'd want to add the mapping in /etc/hosts: {result[0].stdout}\tsharelatex.local")

    return roles, networks


def vagrant():
    conf = (
        en.VagrantConf()
        .add_machine(roles=["overleaf"], flavour="large", number=1)
        .add_machine(roles=["apache"], flavour="medium", number=1)
        .add_network(roles=["prod"], cidr="192.168.42.0/24")
    )
    provider = en.Vagrant(conf)
    roles, networks = provider.init()

    registry_opts = {}
    deploy(roles, networks, registry_opts)
    apache(roles, networks)

    return roles, networks


def apache(roles, networks):
    """Deploy apache2"""
    roles = en.sync_info(roles, networks)
    vhost_address = roles["overleaf"][0].filter_addresses(networks["prod"])
    vhost_address = str(vhost_address[0].ip.ip)
    with en.actions(roles=roles["apache"]) as a:
        a.apt(name=["apache2", "libapache2-mod-security2"], state="present")
        a.apache2_module(name="proxy_http")
        a.apache2_module(name="rewrite")
        a.copy(dest="/etc/apache2/sites-available/overleaf.conf", content=f"""
<VirtualHost *:80>
    ServerName example.com

    ProxyPass / http://{vhost_address}/ nocanon
    ProxyPassReverse / http://{vhost_address}/

    RewriteEngine on
    RewriteCond %{{HTTP:Upgrade}} websocket [NC]
    RewriteCond %{{HTTP:Connection}} upgrade [NC]
    RewriteRule ^/?(.*) "ws://{vhost_address}/$1" [P,L]
</VirtualHost>
        """)
        a.shell("a2ensite overleaf")
        # not idempotent
        a.shell("a2dissite *default* || true")
        # detection only mod_security (this is the default)
        a.shell("cp /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf")
        a.service(name="apache2", state="restarted")


def populate(roles):
    with en.actions(roles=roles["overleaf"]) as a:
        a.shell("""
cd /tmp/
wget https://gitlab.inria.fr/sed-rennes/sharelatex/python-sharelatex/-/raw/main/sharelatex/tests/create-user-with-pass.js
docker cp create-user-with-pass.js  sharelatex:/var/www/sharelatex/web/modules/server-ce-scripts/scripts
for i in {1..20}; 
    do
    docker exec --workdir /var/www/sharelatex/web/modules/server-ce-scripts/scripts -i sharelatex node create-user-with-pass.js --email=joe$i@inria.fr --pass=TestTest42; 
done
        """)
        
if __name__ == "__main__":
    roles, networks = g5k()
    populate(roles)
    