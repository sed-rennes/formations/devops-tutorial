variable "g5k_username" {
  description = "G5K username for API access"
  type        = string
  sensitive   = true
}

variable "g5k_password" {
  description = "G5K password for API access"
  type        = string
  sensitive   = true
}
