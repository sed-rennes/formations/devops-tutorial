#!/bin/bash
#OAR -l core=1,walltime=00:10:00
#OAR --array 30
#OAR -O /srv/tempdd/{{USER}}/array_job.%jobid%.output
#OAR -E /srv/tempdd/{{USER}}/array_job.%jobid%.error
#set -xv
echo OAR_JOB_ID : $OAR_JOB_ID
echo OAR_ARRAY_INDEX: $OAR_ARRAY_INDEX
echo $OAR_NODEFILE :
cat $OAR_NODEFILE

echo "pwd :"

pwd

. /etc/profile.d/modules.sh
. ./env_overleaf/bin/activate
#export URL_LOGIN="https://test-overleaf.irisa.fr"
export URL_LOGIN="https://qlf-overleaf.irisa.fr"
echo "`date` $OAR_ARRAY_INDEX begin"
python activity.py
echo "`date` $OAR_ARRAY_INDEX end"
