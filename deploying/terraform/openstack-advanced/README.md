# About

This is a more advanced Terraform example which uses a custom Terraform module,
and creates several instances of this module.

The use-case is to create virtual machines for students on an OpenStack cluster:
each group of student has an independant OpenStack project/tenant, and we want
to create identical VMs for each tenant.

# Getting started

## First time install

- Install Terraform
- Run `terraform init`
- Change project names and auth URL in `main.tf`

## Creating / recreating VMs

- `export OS_USERNAME="yourusername"`
- `export OS_PASSWORD="yourpassword"`
- Run `terraform plan`, `terraform apply`
- The floating IP of all VMs should be printed on stdout
- You can SSH with `ssh ubuntu@<floating_ip>` with your regular RSA SSH key
- Run `terraform destroy` to release VMs

