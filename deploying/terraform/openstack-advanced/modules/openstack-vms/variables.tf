variable "group_name" {
  description = "Name of the group (OpenStack project name)"
  type        = string
}

variable "nb_vms" {
  description = "Number of VMs to create."
  type        = number
}

variable "root_password" {
  description = "Password to setup as root for SSH."
  type        = string
  sensitive   = true
}

variable "network_pool_name" {
  description = "Name of the network pool from which to allocation the floating IPs"
  type        = string
}
