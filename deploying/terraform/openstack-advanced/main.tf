# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

# NOTE: you need to define OS_USERNAME and OS_PASSWORD before calling terraform

provider "openstack" {
  tenant_name = "your_project"
  auth_url    = "https://your-openstack-instance-url"
}

# SSH key pair: use the local one
resource "openstack_compute_keypair_v2" "sshkey" {
  name       = "sshkey"
  public_key = file("~/.ssh/id_rsa.pub")
}

variable "vms_per_group" {
  description = "Number of VMs to create for each group"
  type        = number
  default     = 1
}

variable "network_pool_name" {
  description = "Name of the network pool from which to allocation the floating IPs"
  type        = string
}


# Use independant openstack provider instances. This is required to be able to
# create objects in different Openstack projects/tenants.
provider "openstack" {
  tenant_name = "subproject-01"
  auth_url    = "https://your-openstack-instance-url"
  alias       = "subproject-01"
}
module "openstack_vms_01" {
  source = "./modules/openstack-vms"
  providers = {
    openstack = openstack.subproject-01
  }
  group_name        = "subproject-01"
  network_pool_name = var.network_pool_name
  nb_vms            = var.vms_per_group
  root_password     = "abcd"
}

provider "openstack" {
  tenant_name = "subproject-02"
  auth_url    = "https://your-openstack-instance-url"
  alias       = "subproject-02"
}
module "openstack_vms_02" {
  source = "./modules/openstack-vms"
  providers = {
    openstack = openstack.subproject-02
  }
  group_name        = "subproject-02"
  network_pool_name = var.network_pool_name
  nb_vms            = var.vms_per_group
  root_password     = "abcd"
}



# Print all IP addresses as output
locals {
  groups = [
    module.openstack_vms_01,
    module.openstack_vms_02,
  ]
}

output "floating_ip_addresses" {
  value = {
    for group in local.groups :
    group.group_name => group.address_list[*].address
  }
}
