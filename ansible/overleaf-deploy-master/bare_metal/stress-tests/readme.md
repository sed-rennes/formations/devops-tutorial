# Prepare test platform for stress test

## deploy a overleaf/sharelatex version with community authentification

edit file `bare-metal/test-overleaf.irisa.fr.yml' to put values (like this exemple):

    sharelatex_site_url: "https://qlf-overleaf.irisa.fr"
    docker_image_overleaf_service: sharelatex/sharelatex:3.0.1


## create fictional pool of users

    ssh root@test-overleaf
    cd /tmp/
    wget https://gitlab.inria.fr/sed-rennes/sharelatex/python-sharelatex/-/raw/main/sharelatex/tests/create-user-with-pass.js
    docker cp create-user-with-pass.js  sharelatex:/var/www/sharelatex/web/modules/server-ce-scripts/scripts
    for i in {1..50}; 
      do
      docker exec --workdir /var/www/sharelatex/web/modules/server-ce-scripts/scripts -i sharelatex node create-user-with-pass.js --email=joe$i@inria.fr --pass=TestTest42; 
    done

## with a first igrida job

launch in interactive mode :

    ssh ${USER}@igrida-frontend.irisa.fr

    oarsub -I -l walltime=0:30:00 

them:

    mkdir -p /srv/tempdd/$USER
    module load spack/py-numpy/1.23.3 spack/py-virtualenv/16.7.6 spack/py-setuptools/65.0.0/
    virtualenv env_overleaf

    . ./env_overleaf/bin/activate

    python -m pip install pyppeteer bs4

run once (to install headless chromium for pyppeteer about 350 Mb)

    python activity.py 

* copy the script `launch_jobs.bash` in your home 

* edit `launch_jobs.bash`

  * replace `{{USER}}` by your user login 
  * adjust value of `URL_LOGIN` environnement variable to point to good url of the service

# launch the test 

connect to igrida-frontend end using a first terminal: 

    ssh ${USER}@igrida-frontend.irisa.fr

them schedule a OAR container (for exemple with 10 different machines and One core per machine) :

    oarsub -I  -t container -l host=10/core=1,walltime=1:00:00 -Y

keep note the number (OAR_ID), you need this in next step. Don't close this terminal until job termination

In other terminal connect to igrida-frontend

    ssh ${USER}@igrida-frontend.irisa.fr

them run array jobs attached to the container with OAR_ID noted in last step :


    oarsub -t inner=OAR_ID  --array 29 -S ./launch_jobs.bash
