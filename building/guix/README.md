# References

- https://guix.gnu.org/
- Bits are inspired from the future MOOC Reproducible Research (to appear) 

# A package ?


`hello` is a simple program that prints `Hello, world!`. The program is written in C.
The corresponding package can be describe using  `guix` commands.

```bash
# general infos
$) guix show hello

# location of the files
$) guix build hello
$) ls $(guix build hello)

# package recipe
$) guix edit hello

# The graph of the package dependencies (build time)
$) guix graph hello | dot -Tpdf > figs/graph_package_hello.pdf
# same for python
$) guix graph python | dot -Tpdf > figs/graph_package_python.pdf

# A more complete graph of the package dependencies (build time)
$) guix graph --type=bag hello | dot -Tpdf > figs/graph_package_hello-full.pdf

# The graph of the outputs
$) guix graph --type=references hello | dot -Tpdf > figs/graph_references_hello.pdf
# same for python
$) guix graph --type=references python | dot -Tpdf > figs/graph_references_python.pdf
```

# An (isolated) environment

```bash
# execute hello in an isolated manner
$) guix shell --container --pure hello -- hello

# execute in an interactive shell
$) guix shell --container --pure hello
(env) hello
Hello, world!
(env) ls
command not found # need coreutils

$) guix shell --container --pure coreutils hello
# look around your fs in this environment (is there something missing ?)
```

All your dependencies can go in a `manifest.scm` file

```bash
$) guix shell --export-manifest hello coreutils > manifest.scm

# environment from the manifest
$) guix shell --container --pure -m manifest.scm


# customize hello package
$) cat manifest_custom_hello.scm
```

# Freezing everything

```
# Get the exact revision used
$) guix describe -f channels > channels.scm

# Feed it to guix pull (e.g on another machine)
$) guix pull -C channels.scm
```
# Creating a Docker environment


```bash
guix pack --format=docker -m manifest_custom_hello.scm

# same as build this returns the actual path to the image (tgz format)

$) docker load $(docker run -ti coreutils-hello:latest hello --version)
Loaded image: coreutils-hello:latest

$)docker run -ti coreutils-hello:latest hello --version
```

# Building a full vm

```bash
# build it
$) guix system vm vm.scm
...
/gnu/store/xrgbm0b9wd1viqsal234hd5zgd4g4jir-run-vm.sh

# same as usual this returns a wrapper script that launches the vm
$) $(guix system vm vm.scm)

```