# About

This is an example Terraform file to reserve resources (i.e. physical
machines) on Grid'5000.

# Getting started

The first time, you need to initiliaze Terraform:

    terraform init

Then, you need to define your G5K username and password to be able to access the API:

    export TF_VAR_g5k_username=your_username
    read -p 'Password? ' -s TF_VAR_g5k_password
    export TF_VAR_g5k_password

You can then create the machines on Grid'5000:

    terraform plan
    terraform apply

It will print the hostname of the reserved G5K node, you can connect to it
over SSH:

    ssh yeti-X.grenoble.g5k

This assumes you have configured SSH on your machine according to
https://www.grid5000.fr/w/SSH

