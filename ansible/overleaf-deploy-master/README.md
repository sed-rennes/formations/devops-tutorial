# Code infrastructure

- [overleaf repository](https://gitlab.inria.fr/sed-rennes/overleaf/overleaf-monorepo)
    - **Description**: wrapper project, fork of the official one.
    - **Purpose**: build all-in-one docker image + dev on the service (this follows the monorepo principles == every code chnage will go there)
    - **Status**: public (AGPL)

- [overleaf-deploy repository](https://gitlab.inria.fr/sed-rennes/overleaf/overleaf-deploy)
    - **Description**: deploy sharelatex using docker-compose
    - **Status**: private

- [compile_worker repository](https://gitlab.inria.fr/sed-rennes/overleaf/compile_worker)
    - **Description**: the docker image used for building overleaf project.
    - **Status**: public
  
- [bare metal](bare_metal) contains Ansible scripts for install from _near bare metal server_ 

# Deployment notes

 Environments

## States (default)

`HOST -> GUEST` mounts list:

- `/var/lib/overleaf -> /var/lib/overleaf`: this is where `overleaf` (www-data(33)) will store its states (files/compilations...)
   Will be writtent using the `www-data(33)` inside the container.
- `/var/lib/mongo:/data/db`: This is where `mongo` stores its states
- `/var/lib/redis:/data/db`: This is where `redis` stores its states
- `/var/lib/overleaf_backup:/backup`: This is where `overleaf_ops` stores the periodic stuffs (backups)

Everything is reasonnably overwritable using docker-compose overriding features.

## Security

- Sharelatex Intra-service communications are HTTP based (e.g. web -> clsi) and
  rely on HTTP basic auth. The password is reset everytime the container is
  restarted (see https://gitlab.inria.fr/sed-rennes/overleaf/overleaf-monorepo/-/blob/12c861eca3a7a408f10792e1ecdc04cf372ad1c7/server-ce/init_scripts/00_regen_sharelatex_secrets.sh)

  An attacker would need to gain access to the intre-service network (docker managed - unexposed) and infer these credentials.

- Mongo is password protected


## Dev

- Create the environment
```bash
cp -r envs/dev.sample envs/dev
```
- Fill it with the right gitlab application settings (e.g  https://gitlab.inria.fr/oauth/applications/85)
  Or create a new one (e.g https://gitlab.inria.fr/oauth/applications).

- Pull the compilation containers:
```bash
# texlive 2020
docker pull registry.gitlab.inria.fr/sed-rennes/overleaf/compile_worker/compile_worker:2020.1001

# texlive 2021
docker pull registry.gitlab.inria.fr/sed-rennes/overleaf/compile_worker/compile_worker:2021.1001
```

- Create the directory structure:
```bash
mkdir -p /var/lib/overleaf/data/user_files
mkdir -p /var/lib/overlead/tmp/uploads
# fix the permissions ? www-data inside the container
```

- Using HTTP is disabled (?), one need to use `https://sharelatex.local` to access the service.
  So `sharelatex.local` must resolve to your localmachine. For instance my
  `/etc/hosts` contains `127.0.0.1       localhost sharelatex.local` The target
  name is customizable by the `VIRTUAL_HOST` variable in the env file.

- Deployment


```bash
ENV=dev make up
```


- Mounts:

```
root@overleaf:/data_overleaf/prod/mongo# cat /etc/fstab
# ...
# skipped content
# ...

# binding for local data path to NFS volume
/data_overleaf/prod/overleaf /var/lib/overleaf none bind
/data_overleaf/prod/mongo /var/lib/mongo none bind
```

- Pull the compilation containers:
```bash
# texlive 2020
docker pull registry.gitlab.inria.fr/sed-rennes/overleaf/compile_worker/compile_worker:2020.1001

# texlive 2021
docker pull registry.gitlab.inria.fr/sed-rennes/overleaf/compile_worker/compile_worker:2021.1001
```

- Create the production environment with production specific settings:

```
envs
└── prod
    ├── certs
    │   ├── overleaf.irisa.fr.crt
    │   └── sharelatex.local.key
    ├── docker-compose.yml       # overrides default depployment: e.g additional containers
    ├── mongo.env                # secret for mongo
    ├── mongo_overleaf.env       # shared secret between sharelatex and mongo
    ├── overleaf.env             # overleaf configuration (e.g docker, smtp ...., custom web ui headers)
```

- Deployment

```bash
ENV=prod make
```
# Some extra Notes

- We build the all-in-one docker image from the (modified) recipes of the monorepo
- The docker image used to compile a project can be
  also changed on a per project basis. this can't be done the ui (but doable
  using the python-sharelatex client). some configuration are required:
  `all_tex_live_docker_image_names` and `all_tex_live_docker_images` must be set
  (used to validate the new name when we set it). `settings.imageroot` must
  also be set in the settings.coffee (it's prepended to the imagename when a
  custom image is set)

## ANSIBLE

see at [bare_metal](bare_metal/README.md) for Ansible deployment 

