# References

- Documentation: https://developer.hashicorp.com/packer
- Installation(amd64): `wget https://releases.hashicorp.com/packer/1.9.4/packer_1.9.4_linux_amd64.zip` 
    - There are many alternatives, but this downloads a statically linked binary to have a quick try


# A small examples

```
# prepare packer plugins
./packer init debian-12.1.0-x86_64.pkr.hcl

# build the image 
./packer build -var 'headless=false' debian-12.1.0-x86_64.pkr.hcl
cd packer-debian-12.1.0-x86_64-qemu

# shrink it
qemu-img convert -O qcow2 debian-12.1.0-x86_64.qcow2 output.qcow2
```

# Misc

- Packer templates for Vagrant boxes: https://github.com/chef/bento
- https://github.com/canonical/packer-maas
