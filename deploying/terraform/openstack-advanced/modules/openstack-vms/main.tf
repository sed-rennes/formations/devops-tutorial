terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

## Base resources

resource "openstack_networking_network_v2" "internal" {
  name           = "internal"
  admin_state_up = "true"
}
resource "openstack_networking_subnet_v2" "subnet" {
  name            = "subnet"
  network_id      = openstack_networking_network_v2.internal.id
  cidr            = "192.168.42.0/24"
  ip_version      = 4
  dns_nameservers = ["192.44.75.10", "192.108.115.2"]
}
resource "openstack_networking_secgroup_v2" "sg-open" {
  name        = "sg-open"
  description = "Open security group, allows everything"
}
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_all" {
  direction         = "ingress"
  ethertype         = "IPv4"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.sg-open.id
}

# External network/router
resource "openstack_networking_router_v2" "router" {
  name                = "router"
  admin_state_up      = true
  external_network_id = "7af4393b-6556-4d29-b68e-107d02709b96" # external
}
resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}

## VMs and their IP addresses

resource "openstack_networking_floatingip_v2" "floating_ips" {
  pool  = var.network_pool_name
  count = var.nb_vms
}

resource "openstack_compute_instance_v2" "openstack_vms" {
  name = "vm${count.index}"
  #image_name      = "ubuntu-22.04"
  image_name      = "imta-ubuntu22"
  flavor_name     = "s10.medium"
  key_pair        = "sshkey"
  security_groups = ["sg-open"]
  network {
    uuid = openstack_networking_network_v2.internal.id
  }
  count = var.nb_vms
}

resource "openstack_compute_floatingip_associate_v2" "ip_associations" {
  floating_ip = openstack_networking_floatingip_v2.floating_ips["${count.index}"].address
  instance_id = openstack_compute_instance_v2.openstack_vms["${count.index}"].id
  count       = var.nb_vms

  connection {
    type = "ssh"
    user = "ubuntu"
    host = self.floating_ip
  }

  provisioner "remote-exec" {
    inline = [
      "echo root:${var.root_password} | sudo chpasswd",
      "echo 'PermitRootLogin yes' | sudo tee -a /etc/ssh/sshd_config",
      "sudo sed -i -e 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config",
      "sudo systemctl restart sshd",
      "sudo mkdir -p /root/.ssh",
      "sudo chmod 700 /root/.ssh",
      "sudo cp /home/ubuntu/.ssh/authorized_keys /root/.ssh/authorized_keys",
    ]
  }
}
