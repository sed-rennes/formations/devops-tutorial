output "group_name" {
  description = "Name of the group (OpenStack project name)"
  value       = var.group_name
}

output "address_list" {
  description = "List of IP addresses of the floating IPs"
  value       = openstack_networking_floatingip_v2.floating_ips
}
