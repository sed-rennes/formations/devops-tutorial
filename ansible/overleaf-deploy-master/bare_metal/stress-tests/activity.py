import asyncio
from pyppeteer import launch
from bs4 import BeautifulSoup
from time import sleep
import os
if "URL_LOGIN" in os.environ:
    url_login = os.environ["URL_LOGIN"]
else:
    url_login = "https://qlf-overleaf.irisa.fr"
if "OAR_ARRAY_INDEX" in os.environ:
    index = os.environ["OAR_ARRAY_INDEX"]
else:
    index=35
email = f"joe{index}@inria.fr"
password="TestTest42"

global_timeout = 120*1000 # in ms


async def hotkey(keyboard,key1,key2):
    await keyboard.down(key1) 
    await keyboard.press(key2) 
    await keyboard.up(key1)

async def activity():
    browser = await launch(headless=True, args= ['--no-sandbox'],ignoreHTTPSErrors=True)
    page = await browser.newPage()
    page.setDefaultNavigationTimeout(global_timeout)
    await page.setJavaScriptEnabled(True)
    await page.setViewport(dict(width=1200, height=1000))
    await page.goto(url_login,{'waitUntil':'domcontentloaded'})
    input= await page.waitForSelector("input[name='email']", { 'visible': True, 'timeout': global_timeout})
    await input.type(email)
    await page.type("input[name='password']",password)
    await page.waitForSelector("button[class='btn-primary btn']")
    await page.click("button[class='btn-primary btn']")
    await page.waitForSelector("div[class='project-list-row']", { 'visible': True, 'timeout': global_timeout})
    print("login OK")

    if await page.querySelector("div[class='project-list-table-name-container']"): 
        html = await page.content()
        soup = BeautifulSoup(html, features="html.parser")
        projects_elements = soup.find_all('td',attrs={'class':"project-list-table-name-cell"})
        if 0 == len(projects_elements):
            # create a project
            await page.click("a[class='btn btn-primary sidebar-new-proj-btn dropdown-toggle']")
            btn = await page.Jx('//a[text()="Example Project"]')
            await btn[0].click()
            dialog= await page.waitForSelector("div[class='modal-dialog']", { 'visible': True, 'timeout': global_timeout})
            await dialog.hover()
            input =await dialog.querySelector("input")
            await input.type('test')
            await page.click("button[class='btn btn-primary']")
            up= await page.waitForSelector("i[class='fa fa-fw fa-level-up']", { 'visible': True, 'timeout': global_timeout})
            print("create new project OK")
            await page.goto(url_login,{'waitUntil':'domcontentloaded'})
    else:
        # create a first project for new account (class are not the same)
        await page.click("a[class='btn btn-success dropdown-toggle']")
        btn = await page.Jx('//a[text()="Example Project"]')
        await btn[0].click()
        dialog= await page.waitForSelector("div[class='modal-dialog']", { 'visible': True, 'timeout': global_timeout})
        await dialog.hover()
        input =await dialog.querySelector("input")
        await input.type('test')
        await page.click("button[class='btn btn-primary']")
        up= await page.waitForSelector("i[class='fa fa-fw fa-level-up']", { 'visible': True, 'timeout': global_timeout})
        print("create new project OK")
        await page.goto(url_login,{'waitUntil':'domcontentloaded'})
    # open first project
    await page.waitForSelector("span[class='project-list-table-name']", { 'visible': True, 'timeout': global_timeout})
    await page.click("a[class='project-list-table-name-link ng-binding']")

    ace_contents= await page.waitForSelector("div[class='ace_content']", { 'visible': True, 'timeout': global_timeout})
    print("Open First Project OK")
    # control + f => open search box
    await hotkey(page.keyboard,'ControlLeft','KeyF')

    # search introduction section 
    await page.keyboard.type("Introduction}",delay=50)

    # quit search box
    await page.keyboard.press("Escape")
    # unselect 
    await page.keyboard.press("ArrowRight")
    await page.keyboard.type("""
hello world
this is a type writing test
""",delay=50)
    # force compilation control + s
    await hotkey(page.keyboard,'ControlLeft','KeyS')
    print("compile after edit text OK")
    sleep(10)
    await browser.close()
asyncio.new_event_loop().run_until_complete(activity()) 