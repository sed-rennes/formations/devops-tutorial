(use-modules (guix packages)
             (gnu packages base)
             (guix download))
             
;; Defining a variant of hello package with an older version
(define hello
  (package
    (inherit hello)
    (version "2.10")
    (source (origin 
              (method url-fetch)
              (uri (string-append "mirror://gnu/hello/hello-" version
                                  ".tar.gz"))
              ;; get the hash guix download mirror://gnu/hello/hello-2.10.tar.gz                   
              (sha256
                (base32
                  "0ssi1wpaf7plaswqqjwigppsg5fyh99vdlb9kzl7c9lng89ndq1i"))))))

;; build up a manifest by concatenating two manifests
;; one from a list of package name
;; another from the package definition
(concatenate-manifests
  (list 
    (specifications->manifest (list "coreutils"))
    (packages->manifest (list hello))
  ))

