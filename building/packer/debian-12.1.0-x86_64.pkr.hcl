packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
  }
}

variable "disk_size" {
  type    = string
  default = "102400"
}

variable "headless" {
  type    = string
  default = "true"
}

variable "http_directory" {
  type    = string
  default = "http/"
}

variable "iso_checksum" {
  type    = string
  default = "9da6ae5b63a72161d0fd4480d0f090b250c4f6bf421474e4776e82eea5cb3143bf8936bf43244e438e74d581797fe87c7193bbefff19414e33932fe787b1400f"
}

variable "iso_checksum_type" {
  type    = string
  default = "sha512"
}

variable "iso_url" {
  type    = string
  default = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.1.0-amd64-netinst.iso"
}

variable "qemu_accelerator" {
  type    = string
  default = "kvm"
}

variable "shutdown_command" {
  type    = string
  default = "echo 'tansiv' | sudo -S shutdown -P now"
}

variable "ssh_password" {
  type    = string
  default = "tansiv"
}

variable "ssh_username" {
  type    = string
  default = "tansiv"
}

variable "template" {
  type    = string
  default = "debian-12.1.0-x86_64"
}

variable "vm_basename" {
  type    = string
  default = "debian-12"
}

source "qemu" "description" {
  accelerator      = "${var.qemu_accelerator}"
  boot_command     = ["<esc><wait>", "install <wait>", " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg <wait>", "debian-installer=en_US.UTF-8 <wait>", "auto <wait>", "locale=en_US.UTF-8 <wait>", "kbd-chooser/method=us <wait>", "keyboard-configuration/xkb-keymap=us <wait>", "netcfg/get_hostname=vm <wait>", "netcfg/get_domain=tansiv <wait>", "fb=false <wait>", "debconf/frontend=noninteractive <wait>", "console-setup/ask_detect=false <wait>", "console-keymaps-at/keymap=us <wait>", "grub-installer/bootdev=/dev/vda <wait>", "<enter><wait>"]
  boot_wait        = "15s"
  disk_size        = "${var.disk_size}"
  headless         = "${var.headless}"
  http_directory   = "${var.http_directory}"
  iso_checksum     = "${var.iso_checksum_type}:${var.iso_checksum}"
  iso_url          = "${var.iso_url}"
  output_directory = "packer-${var.template}-qemu"
  qemuargs         = [["-m", "1024M"]]
  shutdown_command = "${var.shutdown_command}"
  ssh_password     = "${var.ssh_password}"
  ssh_timeout      = "10000s"
  ssh_username     = "${var.ssh_username}"
  vm_name          = "${var.template}.qcow2"
}

build {
  description = "Debian 12 amd64"

  sources = ["source.qemu.description"]

  provisioner "shell" {
    execute_command = "echo 'tansiv' | {{ .Vars }} sudo -S -E sh -eux '{{ .Path }}'"
    scripts         = ["scripts/init.sh", "scripts/post-install.sh", "scripts/minimize.sh"]
  }

}
