# Ansible Playbook for overleaf deployment 


* All in one

```bash
ansible-playbook --timeout 300 -e @secret.yaml -i test-overleaf.irisa.fr.yml site.yml

```

* Proxy only

```bash

ansible-playbook --timeout 300 --tags proxy -e @secret.yaml -i test-overleaf.irisa.fr.yml site.yml
```
Example of secrets:

```yaml
gitlab_app_id:  XXX
gitlab_app_secret: YYY
mongo_sharelatex_password: ZZZ
sharelatex_bot_password: MMM
mongo_initdb_root_password : TTT
```

 __Remarque__: if you are connecting throw a ssh bastion or proxy, maybe you need to redefine timeout


 ## Architecture overview (IRISA):

 ```mermaid
graph LR


subgraph DMZ[DMZ]
    qlf-overleaf.irisa.fr -- alias DNS--> proxy1
    overleaf.irisa.fr -- alias DNS--> proxy2

    test[test-overleaf.irisa.fr]
    subgraph RP[Reverse proxy]
        proxy1[proxy-overleaf.irisa.fr]
        proxy2[proxy-overleaf.irisa.fr]
    end
    style RP color:#f66
end

subgraph internet
    client[python-sharelatex]
    user[user browser]
end


subgraph intranet
    prod[prod-overleaf.irisa.fr]
end

user-- https:443 --> overleaf.irisa.fr
client-- https:443 --> overleaf.irisa.fr
proxy2-- reverse proxy : 443-->prod
proxy1-- reverse proxy : 443-->test
```
